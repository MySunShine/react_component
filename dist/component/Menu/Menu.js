import React from "react";
import "../../index.css";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
export var Menu = function (_a) {
    var _b = _a.direction, direction = _b === void 0 ? "horizontal" : _b, className = _a.className, style = _a.style, items = _a.items, defaultKey = _a.defaultKey, props = __rest(_a, ["direction", "className", "style", "items", "defaultKey"]);
    return (React.createElement("ul", __assign({ className: ["navbar-nav", className].join(" "), style: style }, props), items && items.length > 0
        ? items.map(function (item) {
            return (React.createElement("li", { key: item.key, className: [
                    "nav-item",
                    "".concat(item.children ? "dropdown" : ""),
                ].join(" ") },
                React.createElement("a", { className: "nav-link", href: "#" },
                    React.createElement("span", { className: "nav-link-title" }, item.label))));
        })
        : null));
};
