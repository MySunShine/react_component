import React from "react";
import "../../index.css";
interface ItemProps {
    title?: string;
    key?: string;
    label?: React.ReactNode;
    icon?: React.ReactNode;
    disabled?: boolean;
    children?: ItemProps[];
}
interface MenuProps {
    direction?: "horizontal" | "vertical" | "inline";
    style?: React.CSSProperties;
    className?: string;
    items?: ItemProps[];
    openKeys?: string[];
    selectedKeys?: string[];
    onSelect?: () => void;
    onClick?: (key: any) => void;
    defaultKey?: string;
}
export declare const Menu: ({ direction, className, style, items, defaultKey, ...props }: MenuProps) => React.JSX.Element;
export {};
