import React from "react";
import "../../index.css";
interface formProp {
    style?: React.CSSProperties;
    className?: string;
    children?: React.ReactNode;
}
export declare const Form: ({ style, className, children }: formProp) => React.JSX.Element;
export declare const FormItem: ({ style, className, children }: formProp) => React.JSX.Element;
export {};
