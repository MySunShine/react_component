import React from "react";
import "../../index.css";
export var Form = function (_a) {
    var style = _a.style, className = _a.className, children = _a.children;
    return (React.createElement("fieldset", { className: ["form", "form-fieldset", className].join(" "), style: style }, children));
};
export var FormItem = function (_a) {
    var style = _a.style, className = _a.className, children = _a.children;
    return (React.createElement("div", { className: ["mb-3", className].join(" "), style: style }, children));
};
