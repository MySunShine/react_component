import React from "react";
import '../../index.css';
export var Alerts = function (_a) {
    var style = _a.style, className = _a.className, type = _a.type, dismissible = _a.dismissible, children = _a.children;
    return (React.createElement("div", { className: [
            "alert",
            className,
            "".concat(type ? "alert-".concat(type) : ""),
            "".concat(dismissible ? "alert-dismissible" : ""),
        ].join(" "), role: "alert", style: style },
        children,
        dismissible ? (React.createElement("a", { className: "btn-close", "data-bs-dismiss": "alert", "aria-label": "close" })) : ("")));
};
