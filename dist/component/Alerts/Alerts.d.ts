import React from "react";
import '../../index.css';
interface AlertsProps {
    children?: React.ReactNode;
    style?: React.CSSProperties;
    type?: "success" | "danger" | "info" | "warning";
    className?: string;
    dismissible?: boolean;
}
export declare const Alerts: ({ style, className, type, dismissible, children }: AlertsProps) => React.JSX.Element;
export {};
