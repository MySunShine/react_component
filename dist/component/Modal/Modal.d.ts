import React from "react";
import "../../index.css";
interface ModalProp {
    style?: React.CSSProperties;
    className?: string;
    open?: boolean;
    title?: React.ReactNode;
    footer?: React.ReactNode;
    onOk?: () => void;
    onCancel?: () => void;
    children?: React.ReactNode;
    okText?: string;
    cancelText?: string;
    closeIcon?: React.ReactNode;
}
export declare const Modal: ({ style, className, open, onCancel, title, footer, onOk, children, okText, cancelText, closeIcon, }: ModalProp) => React.JSX.Element;
export {};
