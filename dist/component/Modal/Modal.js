import React from "react";
import "../../index.css";
import Button from "../Button";
export var Modal = function (_a) {
    var style = _a.style, className = _a.className, open = _a.open, onCancel = _a.onCancel, title = _a.title, footer = _a.footer, onOk = _a.onOk, children = _a.children, okText = _a.okText, cancelText = _a.cancelText, closeIcon = _a.closeIcon;
    return (React.createElement("div", { className: ["modal", open ? "showModal" : null, className].join(" "), style: style, role: "dialog", "aria-modal": "true", id: "exampleModal" },
        React.createElement("div", { className: "modal-dialog", role: "document" },
            React.createElement("div", { className: "modal-content" },
                React.createElement("div", { className: "modal-header" },
                    React.createElement("h5", { className: "modal-title" }, title),
                    closeIcon === null ? ("") : closeIcon ? (closeIcon) : (React.createElement("button", { type: "button", className: "btn-close", "data-bs-dismiss": "modal", "aria-label": "Close", onClick: onCancel ? function () { return onCancel(); } : function () { } }))),
                React.createElement("div", { className: "modal-body" }, children),
                footer === null ? ("") : (React.createElement("div", { className: "modal-footer" },
                    React.createElement(Button, { color: "white", onClick: onCancel ? function () { return onCancel(); } : function () { } }, cancelText ? cancelText : "取消"),
                    React.createElement(Button, { type: "primary", onClick: onOk ? function () { return onOk(); } : function () { } }, okText ? okText : "确定")))))));
};
