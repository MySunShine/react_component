import React from "react";
import '../../index.css';
export var Badges = function (_a) {
    var style = _a.style, className = _a.className, bgcolor = _a.bgcolor, textcolor = _a.textcolor, pill = _a.pill, softcolor = _a.softcolor, outline = _a.outline, children = _a.children;
    return (React.createElement("span", { style: style, className: [
            "badge",
            className,
            "".concat(bgcolor ? "bg-".concat(bgcolor) : ""),
            "".concat(outline ? "badge-outline" : ""),
            "".concat(textcolor ? "text-".concat(textcolor) : ""),
            "".concat(softcolor ? "bg-".concat(softcolor, "-lt") : ""),
            "".concat(pill ? "badge-pill" : ""),
        ].join(" ") }, children));
};
