import React from "react";
import '../../index.css';
interface BadgesProps {
    children?: React.ReactNode;
    style?: React.CSSProperties;
    className?: string;
    bgcolor?: string;
    textcolor?: string;
    pill?: boolean;
    softcolor?: string;
    outline?: boolean;
}
export declare const Badges: ({ style, className, bgcolor, textcolor, pill, softcolor, outline, children, }: BadgesProps) => React.JSX.Element;
export {};
