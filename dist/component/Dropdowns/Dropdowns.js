import React, { useState } from "react";
import '../../index.css';
export var Dropdowns = function (_a) {
    var style = _a.style, className = _a.className, value = _a.value, defaultValue = _a.defaultValue, Items = _a.Items, menuHead = _a.menuHead, arrow = _a.arrow, dark = _a.dark, onChange = _a.onChange, children = _a.children;
    var _b = useState(false), show = _b[0], setShow = _b[1];
    var _c = useState(value), ckey = _c[0], setckey = _c[1];
    var showMenu = function () {
        setShow(!show);
    };
    var selectKey = function (key) {
        setckey(key);
        setShow(!show);
    };
    return (React.createElement("div", { className: ["dropdown", className].join(" "), style: style },
        React.createElement("div", { className: ["btn", "dropdown-toggle", "".concat(show ? "show" : "")].join(" "), "data-bs-toggle": "dropdown", onClick: function () { return showMenu(); } }, ckey ? ckey : defaultValue ? defaultValue : ""),
        React.createElement("div", { className: [
                "dropdown-menu",
                "".concat(arrow ? "dropdown-menu-arrow" : ""),
                "".concat(dark ? "bg-dark text-white" : ""),
                "".concat(show ? "show" : ""),
            ].join(" "), onClick: onChange ? function () { return onChange(ckey); } : function () { } },
            menuHead ? React.createElement("span", { className: "dropdown-header" }, menuHead) : "",
            Items && Items.length > 0
                ? Items.map(function (item) {
                    return (React.createElement("div", { className: [
                            "dropdown-item",
                            "".concat(ckey && ckey === item.value ? "active" : ""),
                            "".concat(item.disabled ? "disabled" : ""),
                        ].join(" "), key: item.value, onClick: function () { return selectKey(item.value); } },
                        item.Icon ? item.Icon : "",
                        item.value,
                        item.child ? item.child : ""));
                })
                : "",
            children ? children : "")));
};
