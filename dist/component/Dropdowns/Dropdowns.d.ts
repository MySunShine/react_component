import React from "react";
import '../../index.css';
interface DropdownProp {
    style?: React.CSSProperties;
    className?: string;
    defaultValue?: string;
    value?: string;
    selectKey?: string;
    arrow?: boolean;
    Items?: {
        label: React.ReactNode;
        value: string;
        Icon?: React.ReactNode;
        child?: React.ReactNode;
        disabled?: boolean;
    }[];
    menuHead?: string;
    dark?: boolean;
    onChange?: (e: any) => void;
    children?: React.ReactNode;
}
export declare const Dropdowns: ({ style, className, value, defaultValue, Items, menuHead, arrow, dark, onChange, children, }: DropdownProp) => React.JSX.Element;
export {};
