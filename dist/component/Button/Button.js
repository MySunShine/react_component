import React from "react";
import "../../index.css";
export var Button = function (_a) {
    var children = _a.children, disabled = _a.disabled, style = _a.style, type = _a.type, size = _a.size, ghost = _a.ghost, outline = _a.outline, pill = _a.pill, className = _a.className, color = _a.color, icon = _a.icon, onClick = _a.onClick;
    return (React.createElement("button", { className: [
            "btn",
            "".concat(disabled ? "disabled" : ""),
            "".concat(size ? "btn-".concat(size) : ""),
            "".concat(type ? "btn-".concat(type) : ""),
            "".concat(color ? "btn-".concat(color) : ""),
            "".concat(ghost ? "btn-ghost-".concat(type) : ""),
            "".concat(ghost ? "btn-ghost-".concat(size) : ""),
            "".concat(ghost ? "btn-ghost-".concat(color) : ""),
            "".concat(pill ? "btn-pill" : ""),
            "".concat(icon ? "btn-icon" : ""),
            "".concat(outline ? "btn-outline-".concat(type) : ""),
            "".concat(outline ? "btn-outline-".concat(size) : ""),
            "".concat(outline ? "btn-outline-".concat(color) : ""),
            className
        ].join(" "), style: style, onClick: onClick ? function () { return onClick(); } : function () { } }, children));
};
