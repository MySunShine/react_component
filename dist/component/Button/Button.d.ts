import React from "react";
import "../../index.css";
interface ButtonProps {
    disabled?: boolean;
    onClick?: () => void;
    children?: React.ReactNode;
    style?: React.CSSProperties;
    type?: "primary" | "secondary" | "success" | "warning" | "danger" | "info" | "dark" | "light";
    className?: string;
    color?: string;
    ghost?: boolean;
    outline?: boolean;
    pill?: boolean;
    size?: "lg" | "sm";
    icon?: boolean;
}
export declare const Button: ({ children, disabled, style, type, size, ghost, outline, pill, className, color, icon, onClick, }: ButtonProps) => React.JSX.Element;
export {};
