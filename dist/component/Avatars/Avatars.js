import React from "react";
import '../../index.css';
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
export var Avatars = function (_a) {
    var style = _a.style, className = _a.className, color = _a.color, children = _a.children, size = _a.size, rounded = _a.rounded, cover = _a.cover;
    return (React.createElement("span", { className: [
            "avatar",
            className,
            "".concat(color ? "bg-".concat(color, "-lt") : ""),
            "".concat(size ? "avatar-".concat(size) : ""),
            "".concat(rounded ? "rounded" : ""),
        ].join(" "), style: __assign(__assign({}, style), { backgroundImage: "url(".concat(cover, ")") }) }, children));
};
export var AvatarsList = function (_a) {
    var className = _a.className, children = _a.children, style = _a.style;
    return (React.createElement("div", { className: ["avatar-list", "avatar-list-stacked", className].join(" "), style: style }, children));
};
