import React from "react";
import '../../index.css';
interface AvatarsProps {
    children?: React.ReactNode;
    style?: React.CSSProperties;
    className?: string;
    color?: string;
    size?: "lg" | "sm" | "xl" | "xs" | "md";
    rounded?: boolean;
    cover?: any;
}
export declare const Avatars: ({ style, className, color, children, size, rounded, cover, }: AvatarsProps) => React.JSX.Element;
export declare const AvatarsList: ({ className, children, style }: AvatarsProps) => React.JSX.Element;
export {};
