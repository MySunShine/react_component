import React from "react";
import '../../index.css';
interface DatagridProp {
    style?: React.CSSProperties;
    children?: React.ReactNode;
    className?: string;
    Items?: {
        title?: React.ReactNode;
        description?: React.ReactNode;
    }[];
}
export declare const Datagrid: ({ style, className, Items }: DatagridProp) => React.JSX.Element;
export {};
