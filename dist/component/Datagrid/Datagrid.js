import React from "react";
import '../../index.css';
export var Datagrid = function (_a) {
    var style = _a.style, className = _a.className, Items = _a.Items;
    return (React.createElement("div", { className: ["datagrid", className].join(" "), style: style }, Items && Items.length > 0
        ? Items.map(function (item, i) {
            return (React.createElement("div", { className: "datagrid-item", key: i },
                item.title ? (React.createElement("div", { className: "datagrid-title" }, item.title)) : (""),
                item.description ? (React.createElement("div", { className: "datagrid-content" }, item.description)) : ("")));
        })
        : ""));
};
