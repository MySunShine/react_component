import React from "react";
import "../../index.css";
interface CardsProps {
    children?: React.ReactNode;
    style?: React.CSSProperties;
    status?: "top" | "start";
    size?: "lg" | "sm" | "xl" | "xs" | "md";
    statuscolor?: string;
    className?: string;
    cover?: any;
    Stacked?: boolean;
    imgPos?: "top" | "bottom";
}
interface CardProps {
    children?: React.ReactNode;
    style?: React.CSSProperties;
    className?: string;
}
export declare const CardTitle: ({ style, className, children }: CardProps) => React.JSX.Element;
export declare const CardBody: ({ style, className, children }: CardProps) => React.JSX.Element;
export declare const CardHead: ({ style, className, children }: CardProps) => React.JSX.Element;
export declare const Cards: ({ style, className, children, status, statuscolor, size, cover, Stacked, imgPos, }: CardsProps) => React.JSX.Element;
export {};
