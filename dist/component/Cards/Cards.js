import React from "react";
import "../../index.css";
export var CardTitle = function (_a) {
    var style = _a.style, className = _a.className, children = _a.children;
    return (React.createElement("div", { className: ["card-title", className].join(" "), style: style }, children));
};
export var CardBody = function (_a) {
    var style = _a.style, className = _a.className, children = _a.children;
    return (React.createElement("div", { className: ["card-body", className].join(" "), style: style }, children));
};
export var CardHead = function (_a) {
    var style = _a.style, className = _a.className, children = _a.children;
    return (React.createElement("div", { className: ["card-header", className].join(" "), style: style }, children));
};
export var Cards = function (_a) {
    var style = _a.style, className = _a.className, children = _a.children, status = _a.status, statuscolor = _a.statuscolor, size = _a.size, cover = _a.cover, Stacked = _a.Stacked, imgPos = _a.imgPos;
    return (React.createElement("div", { className: [
            "card",
            className,
            "".concat(size ? "card-status-".concat(size) : ""),
            "".concat(Stacked ? "card-stacked" : ""),
        ].join(" "), style: style },
        status ? (React.createElement("div", { className: [
                "card-status-".concat(status),
                "".concat(statuscolor ? "bg-".concat(statuscolor) : ""),
            ].join(" ") })) : (""),
        cover ? (React.createElement("div", { className: [
                "img-responsive",
                "img-responsive-21x9",
                "card-img-".concat(imgPos),
            ].join(" "), style: { backgroundImage: "url(".concat(cover, ")") } })) : (""),
        children));
};
