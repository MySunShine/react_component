import React from "react";
import '../../index.css';
interface AutosizeProps {
    onClick?: () => void;
    children?: React.ReactNode;
    style?: React.CSSProperties;
    className?: string;
    placeholder?: string;
}
export declare const AutoSize: ({ style, className, placeholder }: AutosizeProps) => React.JSX.Element;
export {};
