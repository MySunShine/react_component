import React from "react";
import '../../index.css';
export var AutoSize = function (_a) {
    var style = _a.style, className = _a.className, placeholder = _a.placeholder;
    return (React.createElement("textarea", { className: ["form-control", className].join(" "), "data-bs-toggle": "autosize", placeholder: placeholder, style: style }));
};
