import "../../index.css";
import React from "react";
// 顶部导航
export var Header = function (_a) {
    var children = _a.children;
    return (React.createElement("header", { className: "navbar navbar-expand-sm navbar-light d-print-none" },
        React.createElement("div", { className: "container-xl" }, children)));
};
// 内容区域
export var Content = function (_a) {
    var children = _a.children;
    return React.createElement("div", { className: "page-body" }, children);
};
export var Container = function (_a) {
    var children = _a.children;
    return React.createElement("div", { className: "page-wrapper" }, children);
};
// 侧边栏
export var Sider = function (_a) {
    var children = _a.children;
    return (React.createElement("aside", { className: "navbar navbar-vertical \r\n      navbar-expand-sm navbar-dark" },
        React.createElement("div", { className: "container-fluid" }, children)));
};
// 页面布局
export var Layout = function (_a) {
    var children = _a.children, _b = _a.theme, theme = _b === void 0 ? 'light' : _b, className = _a.className, style = _a.style;
    return (React.createElement("div", { className: ["page", className].join(" "), "data-bs-theme": theme, style: style }, children));
};
// 普通布局
export var commonLayout = function () {
    return (React.createElement("div", { className: "page" },
        React.createElement("header", { className: "navbar navbar-expand-sm navbar-light d-print-none" },
            React.createElement("div", { className: "container-xl" },
                React.createElement("h1", { className: "navbar-brand navbar-brand-autodark d-none-navbar-horizontal pe-0 pe-md-3" },
                    React.createElement("a", { href: "#" },
                        React.createElement("img", { src: "...", width: "110", height: "32", alt: "Tabler", className: "navbar-brand-image" }))),
                React.createElement("div", { className: "navbar-nav flex-row order-md-last" },
                    React.createElement("div", { className: "nav-item" },
                        React.createElement("a", { href: "#", className: "nav-link d-flex lh-1 text-reset p-0" },
                            React.createElement("span", { className: "avatar avatar-sm", style: { backgroundImage: "url(...)" } }),
                            React.createElement("div", { className: "d-none d-xl-block ps-2" },
                                React.createElement("div", null, "Pawe\u0142 Kuna"),
                                React.createElement("div", { className: "mt-1 small text-secondary" }, "UI Designer"))))))),
        React.createElement("div", { className: "page-wrapper" },
            React.createElement("div", { className: "page-body" },
                React.createElement("div", { className: "container-xl" },
                    React.createElement("div", { className: "row row-deck row-cards" },
                        React.createElement("div", { className: "col-4" },
                            React.createElement("div", { className: "card" },
                                React.createElement("div", { className: "card-body", style: { height: "10rem" } }))),
                        React.createElement("div", { className: "col-4" },
                            React.createElement("div", { className: "card" },
                                React.createElement("div", { className: "card-body", style: { height: "10rem" } }))),
                        React.createElement("div", { className: "col-4" },
                            React.createElement("div", { className: "card" },
                                React.createElement("div", { className: "card-body", style: { height: "10rem" } }))),
                        React.createElement("div", { className: "col-12" },
                            React.createElement("div", { className: "card" },
                                React.createElement("div", { className: "card-body", style: { height: "10rem" } })))))))));
};
// 含侧边栏的布局
export var SiderLayout = function () {
    return (React.createElement("div", { className: "page" },
        React.createElement("aside", { className: "navbar navbar-vertical \r\n      navbar-expand-sm navbar-dark" },
            React.createElement("div", { className: "container-fluid" },
                React.createElement("button", { className: "navbar-toggler", type: "button" },
                    React.createElement("span", { className: "navbar-toggler-icon" })),
                React.createElement("h1", { className: "navbar-brand navbar-brand-autodark" },
                    React.createElement("a", { href: "#" },
                        React.createElement("img", { src: "https://preview.tabler.io/static/logo-white.svg", width: "110", height: "32", alt: "Tabler", className: "navbar-brand-image" }))),
                React.createElement("div", { className: "collapse navbar-collapse", id: "sidebar-menu" },
                    React.createElement("ul", { className: "navbar-nav pt-lg-3" },
                        React.createElement("li", { className: "nav-item" },
                            React.createElement("a", { className: "nav-link", href: "#" },
                                React.createElement("span", { className: "nav-link-title" }, "Home"))),
                        React.createElement("li", { className: "nav-item" },
                            React.createElement("a", { className: "nav-link", href: "#" },
                                React.createElement("span", { className: "nav-link-title" }, "Link 1"))),
                        React.createElement("li", { className: "nav-item" },
                            React.createElement("a", { className: "nav-link", href: "#" },
                                React.createElement("span", { className: "nav-link-title" }, "Link 2"))),
                        React.createElement("li", { className: "nav-item" },
                            React.createElement("a", { className: "nav-link", href: "#" },
                                React.createElement("span", { className: "nav-link-title" }, "Link 3"))))))),
        React.createElement("div", { className: "page-wrapper" },
            React.createElement("div", { className: "page-header d-print-none" },
                React.createElement("div", { className: "container-xl" },
                    React.createElement("div", { className: "row g-2 align-items-center" },
                        React.createElement("div", { className: "col" },
                            React.createElement("h2", { className: "page-title" }, "Vertical layout"))))),
            React.createElement("div", { className: "page-body" },
                React.createElement("div", { className: "container-xl" },
                    React.createElement("div", { className: "row row-deck row-cards" },
                        React.createElement("div", { className: "col-sm-6 col-lg-3" },
                            React.createElement("div", { className: "card" },
                                React.createElement("div", { className: "card-body", style: { height: "10rem" } }))),
                        React.createElement("div", { className: "col-sm-6 col-lg-3" },
                            React.createElement("div", { className: "card" },
                                React.createElement("div", { className: "card-body", style: { height: "10rem" } }))),
                        React.createElement("div", { className: "col-sm-6 col-lg-3" },
                            React.createElement("div", { className: "card" },
                                React.createElement("div", { className: "card-body", style: { height: "10rem" } }))),
                        React.createElement("div", { className: "col-sm-6 col-lg-3" },
                            React.createElement("div", { className: "card" },
                                React.createElement("div", { className: "card-body", style: { height: "10rem" } }))),
                        React.createElement("div", { className: "col-lg-6" },
                            React.createElement("div", { className: "row row-cards" },
                                React.createElement("div", { className: "col-12" },
                                    React.createElement("div", { className: "card" },
                                        React.createElement("div", { className: "card-body", style: { height: "10rem" } }))),
                                React.createElement("div", { className: "col-12" },
                                    React.createElement("div", { className: "card" },
                                        React.createElement("div", { className: "card-body", style: { height: "10rem" } }))))),
                        React.createElement("div", { className: "col-lg-6" },
                            React.createElement("div", { className: "card" },
                                React.createElement("div", { className: "card-body", style: { height: "10rem" } }))),
                        React.createElement("div", { className: "col-12" },
                            React.createElement("div", { className: "card" },
                                React.createElement("div", { className: "card-body", style: { height: "10rem" } }))),
                        React.createElement("div", { className: "col-md-12 col-lg-8" },
                            React.createElement("div", { className: "card" },
                                React.createElement("div", { className: "card-body", style: { height: "10rem" } }))),
                        React.createElement("div", { className: "col-md-6 col-lg-4" },
                            React.createElement("div", { className: "card" },
                                React.createElement("div", { className: "card-body", style: { height: "10rem" } }))),
                        React.createElement("div", { className: "col-md-6 col-lg-4" },
                            React.createElement("div", { className: "card" },
                                React.createElement("div", { className: "card-body", style: { height: "10rem" } }))),
                        React.createElement("div", { className: "col-md-12 col-lg-8" },
                            React.createElement("div", { className: "card" },
                                React.createElement("div", { className: "card-body", style: { height: "10rem" } }))),
                        React.createElement("div", { className: "col-12" },
                            React.createElement("div", { className: "card" },
                                React.createElement("div", { className: "card-body", style: { height: "10rem" } })))))))));
};
