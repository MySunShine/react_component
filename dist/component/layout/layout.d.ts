import "../../index.css";
import React, { CSSProperties } from "react";
interface LayoutProps {
    children?: React.ReactNode;
    style?: CSSProperties;
    className?: string;
    theme?: string;
}
export declare const Header: ({ children }: LayoutProps) => React.JSX.Element;
export declare const Content: ({ children }: LayoutProps) => React.JSX.Element;
export declare const Container: ({ children }: LayoutProps) => React.JSX.Element;
export declare const Sider: ({ children }: LayoutProps) => React.JSX.Element;
export declare const Layout: ({ children, theme, className, style }: LayoutProps) => React.JSX.Element;
export declare const commonLayout: () => React.JSX.Element;
export declare const SiderLayout: () => React.JSX.Element;
export {};
