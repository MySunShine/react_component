import React from "react";
import "../../index.css";
export var Input = function (_a) {
    var style = _a.style, className = _a.className, placeholder = _a.placeholder, onChange = _a.onChange, size = _a.size, rounded = _a.rounded, flush = _a.flush, textPos = _a.textPos;
    return (React.createElement("input", { type: "text", placeholder: placeholder, className: [
            className,
            "form-control",
            "".concat(rounded ? "form-control-rounded" : null),
            "".concat(flush ? "form-control-flush" : null),
            "".concat(size ? "form-control-".concat(size) : null),
            "".concat(textPos ? "text-".concat(textPos) : null),
        ].join(" "), style: style, onInput: onChange ? function (e) { return onChange(e); } : function () { } }));
};
export var InputText = function () {
    return React.createElement("div", { className: "input-group" });
};
export var InputIconGroup = function (_a) {
    var children = _a.children;
    return React.createElement("div", { className: "input-icon" }, children);
};
export var InputIcon = function (_a) {
    var children = _a.children;
    return React.createElement("span", { className: "input-icon-addon" }, children);
};
