import React from "react";
import "../../index.css";
interface InputProp {
    onChange?: (e: any) => void;
    style?: React.CSSProperties;
    className?: string;
    placeholder?: string;
    size?: "lg" | "sm" | "xl" | "xs" | "md";
    rounded?: boolean;
    flush?: boolean;
    textPos?: "end" | "center" | "start";
}
interface groupProp {
    children?: React.ReactNode;
}
export declare const Input: ({ style, className, placeholder, onChange, size, rounded, flush, textPos, }: InputProp) => React.JSX.Element;
export declare const InputText: () => React.JSX.Element;
export declare const InputIconGroup: ({ children }: groupProp) => React.JSX.Element;
export declare const InputIcon: ({ children }: groupProp) => React.JSX.Element;
export {};
