import type { Meta, StoryObj } from "@storybook/react";
import ea from "../../images/ava.jpg";
import ea1 from "../../images/ava1.jpg";
import { Classify } from "./Classify";

const meta: Meta<typeof Classify> = {
  title: "页面/Classify 分类页",
  component: Classify,
  argTypes: {
    style: { description: "指定样式" },
    className: {
      description: "容器 className",
    },
    Items: {
      description: "分类菜单配置项",
    },
    onChange: {
      description: "点击每个菜单回调函数,返回配置项key值",
    },
  },
};
export default meta;

type Story = StoryObj<typeof meta>;
export const simpleClassify: Story = {
  render: () => (
    <Classify
      Items={[
        {
          Category: "分类一",
          children: [
            {
              img: ea,
              title: "收发管理",
              introduction: "介绍:1",
              key: "reciveAndforward",
            },
            {
              img: ea,
              title: "解析管理",
              introduction: "介绍:2",
              key: "parse",
            },
            { img: ea, title: "存储管理", introduction: "介绍:3", key: "save" },
          ],
        },
        {
          Category: "分类二",
          children: [
            {
              img: ea1,
              title: "查询管理",
              introduction: "介绍:1",
              key: "search",
            },
            {
              img: ea1,
              title: "信息管理",
              introduction: "介绍:2",
              key: "message",
            },
            {
              img: ea1,
              title: "任务管理",
              introduction: "介绍:3",
              key: "work",
            },
            {
              img: ea1,
              title: "监显管理",
              introduction: "介绍:4",
              key: "watch",
            },
          ],
        },
        {
          Category: "分类三",
          children: [
            { img: ea, title: "运维管理", introduction: "介绍:1", key: "run" },
          ],
        },
      ]}
      onChange={() => {}}
    />
  ),
};
