import React from "react";
import "../../index.css";
import { TRCards, TRCardBody, TRCardTitle } from "../Cards";
interface ClassifyProp {
  style?: React.CSSProperties;
  className?: string;
  Items?: {
    Category?: React.ReactNode;
    children: {
      key?: string;
      img?: string;
      title?: React.ReactNode;
      introduction?: React.ReactNode;
    }[];
  }[];
  onChange?: (e: any) => void;
}
export const Classify = ({
  style,
  className,
  Items,
  onChange,
}: ClassifyProp) => {
  return (
    <TRCards style={style} className={className}>
      <TRCardBody>
        {Items && Items.length > 0
          ? Items.map((item, i) => {
              return (
                <div key={i}>
                  <div
                    className="hr-text hr-text-left"
                    style={{ margin: "20px 0", border: 0 }}
                  >
                    {item.Category}
                  </div>
                  <div className="flex_start">
                    {item.children.map((item1, i1) => {
                      return (
                        <div
                          className="card"
                          key={i1}
                          onClick={
                            onChange ? () => onChange(item1.key) : () => {}
                          }
                          style={{ width: "40%", margin: "2% 5%" }}
                        >
                          <div className="row">
                            <div className="col-3">
                              <img
                                src={item1.img}
                                alt="图片异常"
                                className="w-100 h-100 object-cover w-100 h-100 object-cover"
                              />
                            </div>
                            <div className="col">
                              <TRCardBody>
                                <TRCardTitle>{item1.title}</TRCardTitle>
                                <p>{item1.introduction}</p>
                              </TRCardBody>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              );
            })
          : ""}
      </TRCardBody>
    </TRCards>
  );
};
