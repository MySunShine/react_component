import React from "react";
import "../../index.css";

interface InputProp {
  onChange?: (e: any) => void;
  style?: React.CSSProperties;
  className?: string;
  placeholder?: string; //初始值
  size?: "lg" | "sm" | "xl" | "xs" | "md"; //大小
  rounded?: boolean;
  flush?: boolean;
  textPos?: "end" | "center" | "start"; //文字位置
}

interface groupProp {
  children?: React.ReactNode;
}

export const TRInput = ({
  style,
  className,
  placeholder,
  onChange,
  size,
  rounded,
  flush,
  textPos,
}: InputProp) => {
  return (
    <input
      type="text"
      placeholder={placeholder}
      className={[
        className,
        "form-control",
        `${rounded ? "form-control-rounded" : null}`,
        `${flush ? "form-control-flush" : null}`,
        `${size ? `form-control-${size}` : null}`,
        `${textPos ? `text-${textPos}` : null}`,
      ].join(" ")}
      style={style}
      onInput={onChange ? (e) => onChange(e) : () => {}}
    />
  );
};

export const InputText = () => {
  return <div className="input-group"></div>;
};

export const InputIconGroup = ({ children }: groupProp) => {
  return <div className="input-icon">{children}</div>;
};

export const InputIcon = ({ children }: groupProp) => {
  return <span className="input-icon-addon">{children}</span>;
};
