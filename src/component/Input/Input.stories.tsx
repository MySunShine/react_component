import type { Meta, StoryObj } from "@storybook/react";

import { TRInput, InputIconGroup, InputIcon } from "./Input";
// import user from "../../Icons/user.svg";
// import lock from "../../Icons/lock.svg";
const meta: Meta<typeof TRInput> = {
  title: "数据录入/input 输入框",
  component: TRInput,
  argTypes: {
    style: { description: "指定样式" },
    className: {
      description: "容器 className",
    },
    placeholder: {
      description: "默认输入值",
    },
    onChange: {
      description: "输入回调函数",
    },
    size: {
      description: "输入框大小",
    },
    rounded: {
      description: "设置输入框圆角状态",
    },
    flush: {
      description: "设置输入框无边框",
    },
  },
};
export default meta;

type Story = StoryObj<typeof meta>;
export const simpleInput: Story = {
  render: () => (
    <div className="space-y">
      <TRInput placeholder="请输入你的问题" size="lg" />
      <TRInput placeholder="请输入你的问题" size="md" />
      <TRInput placeholder="请输入你的问题" size="sm" />
      <TRInput placeholder="请输入" size="md" rounded={true} />
      <TRInput placeholder="请输入" size="md" flush={true} />
      <InputIconGroup>
        <InputIcon>
          {/* <img src={user} alt="user" /> */}
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-user"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            stroke-width="2"
            stroke="currentColor"
            fill="none"
            stroke-linecap="round"
            stroke-linejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M8 7a4 4 0 1 0 8 0a4 4 0 0 0 -8 0"></path>
            <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
          </svg>
        </InputIcon>
        <TRInput placeholder="请输入你的问题" size="md" />
      </InputIconGroup>
      <InputIconGroup>
        <InputIcon>
          {/* <img
            src={lock}
            alt="user"
          /> */}
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-lock"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M5 13a2 2 0 0 1 2 -2h10a2 2 0 0 1 2 2v6a2 2 0 0 1 -2 2h-10a2 2 0 0 1 -2 -2v-6z"></path>
            <path d="M11 16a1 1 0 1 0 2 0a1 1 0 0 0 -2 0"></path>
            <path d="M8 11v-4a4 4 0 1 1 8 0v4"></path>
          </svg>
        </InputIcon>
        <TRInput placeholder="请输入你的问题" size="md" />
      </InputIconGroup>
    </div>
  ),
};
