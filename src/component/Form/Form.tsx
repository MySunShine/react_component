import React from "react";
import "../../index.css";
interface formProp {
  style?: React.CSSProperties;
  className?: string;
  children?: React.ReactNode;
}
export const TRForm = ({ style, className, children }: formProp) => {
  return (
    <fieldset
      className={["form", "form-fieldset", className].join(" ")}
      style={style}
    >
      {children}
    </fieldset>
  );
};

export const TRFormItem = ({ style, className, children }: formProp) => {
  return (
    <div className={["mb-3", className].join(" ")} style={style}>
      {children}
    </div>
  );
};
