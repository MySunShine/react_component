import type { Meta, StoryObj } from "@storybook/react";

import { TRForm, TRFormItem } from "./Form";
import Button from "../Button";
import { TRCards, TRCardBody, TRCardTitle } from "../Cards";
import { TRInput, InputIconGroup, InputIcon } from "../Input";
const meta: Meta<typeof TRForm> = {
  title: "数据录入/Form 表单",
  component: TRForm,
  argTypes: {
    style: { description: "指定样式" },
    className: {
      description: "容器 className",
    },
  },
};
export default meta;

type Story = StoryObj<typeof meta>;
export const simpleForm: Story = {
  render: () => (
    <TRForm style={{ width: 260 }}>
      <TRFormItem>
        <h3>账号密码登录</h3>
      </TRFormItem>
      <TRFormItem>
        <InputIconGroup>
          <InputIcon>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="icon icon-tabler icon-tabler-user"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              stroke-width="2"
              stroke="currentColor"
              fill="none"
              stroke-linecap="round"
              stroke-linejoin="round"
            >
              <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
              <path d="M8 7a4 4 0 1 0 8 0a4 4 0 0 0 -8 0"></path>
              <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
            </svg>
          </InputIcon>
          <TRInput placeholder="请输入用户名" size="md" />
        </InputIconGroup>
      </TRFormItem>
      <TRFormItem>
        <InputIconGroup>
          <InputIcon>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="icon icon-tabler icon-tabler-lock"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              strokeWidth="2"
              stroke="currentColor"
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
            >
              <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
              <path d="M5 13a2 2 0 0 1 2 -2h10a2 2 0 0 1 2 2v6a2 2 0 0 1 -2 2h-10a2 2 0 0 1 -2 -2v-6z"></path>
              <path d="M11 16a1 1 0 1 0 2 0a1 1 0 0 0 -2 0"></path>
              <path d="M8 11v-4a4 4 0 1 1 8 0v4"></path>
            </svg>
          </InputIcon>
          <TRInput placeholder="请输入用户密码" size="md" />
        </InputIconGroup>
      </TRFormItem>
      <TRFormItem>
        <Button style={{ width: "100%" }} color="indigo">
          登录
        </Button>
      </TRFormItem>
    </TRForm>
  ),
};

export const simpleLogin: Story = {
  render: () => (
    <TRCards style={{ width: 360 }}>
      <TRCardBody>
        <TRCardTitle>Login to your Account</TRCardTitle>
        <TRFormItem>
          <label className="form-label">Username</label>
          <TRInput placeholder="username" size="md" />
        </TRFormItem>
        <TRFormItem>
          <label className="form-label">Password</label>
          <TRInput placeholder="password" size="md" />
        </TRFormItem>
        <TRFormItem>
          <Button style={{ width: "100%" }} color="indigo">
            login
          </Button>
        </TRFormItem>
      </TRCardBody>
    </TRCards>
  ),
};
