import type { Meta, StoryObj } from "@storybook/react";

import { TRMenu } from "./Menu";

const meta: Meta<typeof TRMenu> = {
  title: "导航/Menu 菜单",
  component: TRMenu,
  argTypes: {
    style: { description: "指定样式" },
    className: {
      description: "容器 className",
    },
  },
};
export default meta;

type Story = StoryObj<typeof meta>;
export const simpleMenu: Story = {
  render: () => <TRMenu />,
};