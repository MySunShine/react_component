import React from "react";
import "../../index.css";
//声明菜单项类型
interface ItemProps {
  title?: string; //设置收缩时展示的悬浮标题
  key?: string; //item唯一标识
  label?: React.ReactNode; //菜单项标题(节点类型，文本节点或标签节点)
  icon?: React.ReactNode; //菜单图标
  disabled?: boolean; //是否禁用
  children?: ItemProps[]; //子菜单的菜单项
}

// 声明菜单类型
interface MenuProps {
  direction?: "horizontal" | "vertical" | "inline"; //菜单方向
  style?: React.CSSProperties;
  className?: string;
  items?: ItemProps[]; //菜单组
  openKeys?: string[];
  selectedKeys?: string[];
  onSelect?: () => void; //被选中时调用
  onClick?: (key: any) => void; //点击选项时调用
  defaultKey?: string;
}

export const TRMenu = ({
  direction = "horizontal",
  className,
  style,
  items,
  defaultKey,
  ...props
}: MenuProps) => {
  return (
    <ul
      className={["navbar-nav", className].join(" ")}
      style={style}
      {...props}
    >
      {items && items.length > 0
        ? items.map((item) => {
            return (
              <li
                key={item.key}
                className={[
                  "nav-item",
                  `${item.children ? "dropdown" : ""}`,
                ].join(" ")}
              >
                <a className="nav-link" href="#">
                  <span className="nav-link-title">{item.label}</span>
                </a>
                {
              
                }
              </li>
            );
          })
        : null}
    </ul>
  );
};
