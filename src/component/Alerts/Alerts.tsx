import React from "react";
import '../../index.css'
interface AlertsProps {
  children?: React.ReactNode;
  style?: React.CSSProperties;
  type?: "success" | "danger" | "info" | "warning";
  className?: string;
  dismissible?: boolean; //关闭效果
}
export const TRAlerts = ({
  style,
  className,
  type,
  dismissible,
  children
}: AlertsProps) => {
  return (
    <div
      className={[
        "alert",
        className,
        `${type ? `alert-${type}` : ""}`,
        `${dismissible ? "alert-dismissible" : ""}`,
      ].join(" ")}
      role="alert"
      style={style}
    >
      {children}
      {dismissible ? (
        <a className="btn-close" data-bs-dismiss="alert" aria-label="close"></a>
      ) : (
        ""
      )}
    </div>
  );
};
