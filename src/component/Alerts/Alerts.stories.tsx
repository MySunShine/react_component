import type { Meta, StoryObj } from "@storybook/react";

import { TRAlerts } from "./Alerts";

const meta: Meta<typeof TRAlerts> = {
  title: "操作反馈/Alerts 提示框",
  component: TRAlerts,
  argTypes: {
    style: { description: "指定样式" },
    className: {
      description: "容器 className",
    },
    type:{
      description:'指定警告提示的样式，有四种选择 success、info、warning、error'
    },
    dismissible:{
      description:'右侧关闭'
    }
  },
};
export default meta;

type Story = StoryObj<typeof meta>;

export const simpleAlerts: Story = {
  render: () => (
    <div>
      <TRAlerts type="success">成功提示</TRAlerts>
      <TRAlerts type="info">信息提示</TRAlerts>
      <TRAlerts type="warning">警告提示</TRAlerts>
      <TRAlerts type="danger">危险提示</TRAlerts>
      <TRAlerts type="success" dismissible={true}>
        成功提示
      </TRAlerts>
      <TRAlerts type="info" dismissible={true}>
        信息提示
      </TRAlerts>
      <TRAlerts type="success" dismissible={true}>
        <h4 className="alert-title">Wow! Everything worked!</h4>
        <div className="text-secondary">Your account has been saved!</div>
      </TRAlerts>
      <TRAlerts type="info" dismissible={true}>
        <h4 className="alert-title">Wow! Everything worked!</h4>
        <div className="text-secondary">Your account has been saved!</div>
      </TRAlerts>
    </div>
  ),
};
