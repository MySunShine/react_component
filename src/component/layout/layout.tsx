import "../../index.css";
import React, { CSSProperties } from "react";
// 定义属性
interface LayoutProps {
  children?: React.ReactNode;
  style?: CSSProperties;
  className?: string; //类名
  theme?: string; //主题
}

// 顶部导航
export const TRHeader = ({ children }: LayoutProps) => {
  return (
    <header className="navbar navbar-expand-sm navbar-light d-print-none">
      <div className="container-xl">{children}</div>
    </header>
  );
};

// 内容区域
export const TRContent = ({ children }: LayoutProps) => {
  return <div className="page-body">{children}</div>;
};

export const TRContainer = ({ children }: LayoutProps) => {
  return <div className="page-wrapper">{children}</div>;
};

// 侧边栏
export const TRSider = ({ children }: LayoutProps) => {
  return (
    <aside
      className="navbar navbar-vertical 
      navbar-expand-sm navbar-dark"
    >
      <div className="container-fluid">{children}</div>
    </aside>
  );
};

// 页面布局
export const TRLayout = ({ children, theme='light', className, style }: LayoutProps) => {
  return (
    <div
      className={["page", className].join(" ")}
      data-bs-theme={theme}
      style={style}
    >
      {children}
    </div>
  );
};

// 普通布局
export const commonLayout = () => {
  return (
    <div className="page">
      <header className="navbar navbar-expand-sm navbar-light d-print-none">
        <div className="container-xl">
          <h1 className="navbar-brand navbar-brand-autodark d-none-navbar-horizontal pe-0 pe-md-3">
            <a href="#">
              <img
                src="..."
                width="110"
                height="32"
                alt="Tabler"
                className="navbar-brand-image"
              />
            </a>
          </h1>

          <div className="navbar-nav flex-row order-md-last">
            <div className="nav-item">
              <a href="#" className="nav-link d-flex lh-1 text-reset p-0">
                <span
                  className="avatar avatar-sm"
                  style={{ backgroundImage: "url(...)" }}
                ></span>
                <div className="d-none d-xl-block ps-2">
                  <div>Paweł Kuna</div>
                  <div className="mt-1 small text-secondary">UI Designer</div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </header>
      <div className="page-wrapper">
        <div className="page-body">
          <div className="container-xl">
            <div className="row row-deck row-cards">
              <div className="col-4">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
              <div className="col-4">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
              <div className="col-4">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
              <div className="col-12">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
// 含侧边栏的布局
export const SiderLayout = () => {
  return (
    <div className="page">
      <aside
        className="navbar navbar-vertical 
      navbar-expand-sm navbar-dark"
      >
        <div className="container-fluid">
          <button className="navbar-toggler" type="button">
            <span className="navbar-toggler-icon"></span>
          </button>
          <h1 className="navbar-brand navbar-brand-autodark">
            <a href="#">
              <img
                src="https://preview.tabler.io/static/logo-white.svg"
                width="110"
                height="32"
                alt="Tabler"
                className="navbar-brand-image"
              />
            </a>
          </h1>
          <div className="collapse navbar-collapse" id="sidebar-menu">
            <ul className="navbar-nav pt-lg-3">
              <li className="nav-item">
                <a className="nav-link" href="#">
                  <span className="nav-link-title">Home</span>
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  <span className="nav-link-title">Link 1</span>
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  <span className="nav-link-title">Link 2</span>
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  <span className="nav-link-title">Link 3</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </aside>
      <div className="page-wrapper">
        <div className="page-header d-print-none">
          <div className="container-xl">
            <div className="row g-2 align-items-center">
              <div className="col">
                <h2 className="page-title">Vertical layout</h2>
              </div>
            </div>
          </div>
        </div>
        <div className="page-body">
          <div className="container-xl">
            <div className="row row-deck row-cards">
              <div className="col-sm-6 col-lg-3">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
              <div className="col-sm-6 col-lg-3">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
              <div className="col-sm-6 col-lg-3">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
              <div className="col-sm-6 col-lg-3">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
              <div className="col-lg-6">
                <div className="row row-cards">
                  <div className="col-12">
                    <div className="card">
                      <div
                        className="card-body"
                        style={{ height: "10rem" }}
                      ></div>
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="card">
                      <div
                        className="card-body"
                        style={{ height: "10rem" }}
                      ></div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-6">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
              <div className="col-12">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
              <div className="col-md-12 col-lg-8">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
              <div className="col-md-6 col-lg-4">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
              <div className="col-md-6 col-lg-4">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
              <div className="col-md-12 col-lg-8">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
              <div className="col-12">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
