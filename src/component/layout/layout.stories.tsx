import type { Meta, StoryObj } from "@storybook/react";

import { TRLayout, TRHeader, TRContent, TRSider, TRContainer } from "./layout";
// import Sun from "../../Icons/sun-high.svg";
// import Moon from "../../Icons/moon.svg";
// import Bell from "../../Icons/bell.svg";
import { TRAvatars } from "../Avatars";
import ea from "../../images/ava.jpg";
import { useState } from "react";
const meta: Meta<typeof TRLayout> = {
  title: "布局/layout 布局",
  component: TRLayout,
};
export default meta;

type Story = StoryObj<typeof meta>;

const ThemeLayout = () => {
  const [theme, setTheme] = useState("light");
  const changeTheme = () => {
    if (theme === "light") {
      setTheme("dark");
    } else {
      setTheme("light");
    }
  };
  return (
    <TRLayout theme={theme}>
      <TRHeader>
        <div>logo</div>
        <div className="navbar-nav" style={{ alignItems: "center" }}>
          <span className={`text-${theme === "light" ? "black" : "white"}`}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="icon icon-tabler icon-tabler-bell"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              strokeWidth="2"
              stroke="currentColor"
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
            >
              <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
              <path d="M10 5a2 2 0 1 1 4 0a7 7 0 0 1 4 6v3a4 4 0 0 0 2 3h-16a4 4 0 0 0 2 -3v-3a7 7 0 0 1 4 -6"></path>
              <path d="M9 17v1a3 3 0 0 0 6 0v-1"></path>
            </svg>
          </span>
          <span
            className={`text-${theme === "light" ? "black" : "white"}`}
            style={{margin:'0 10px'}}
            onClick={() => changeTheme()}
          >
            {theme === "light" ? (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="icon icon-tabler icon-tabler-moon"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                strokeWidth="2"
                stroke="currentColor"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <path d="M12 3c.132 0 .263 0 .393 0a7.5 7.5 0 0 0 7.92 12.446a9 9 0 1 1 -8.313 -12.454z"></path>
              </svg>
            ) : (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="icon icon-tabler icon-tabler-sun-high"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                strokeWidth="2"
                stroke="currentColor"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <path d="M14.828 14.828a4 4 0 1 0 -5.656 -5.656a4 4 0 0 0 5.656 5.656z"></path>
                <path d="M6.343 17.657l-1.414 1.414"></path>
                <path d="M6.343 6.343l-1.414 -1.414"></path>
                <path d="M17.657 6.343l1.414 -1.414"></path>
                <path d="M17.657 17.657l1.414 1.414"></path>
                <path d="M4 12h-2"></path>
                <path d="M12 4v-2"></path>
                <path d="M20 12h2"></path>
                <path d="M12 20v2"></path>
              </svg>
            )}
          </span>
          <TRAvatars cover={ea}></TRAvatars>
        </div>
      </TRHeader>
      <TRContainer>
        <TRContent>
          <div className="container-xl">
            <div className="row row-deck row-cards">
              <div className="col-4">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
              <div className="col-4">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
              <div className="col-4">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
              <div className="col-12">
                <div className="card">
                  <div className="card-body" style={{ height: "10rem" }}></div>
                </div>
              </div>
            </div>
          </div>
        </TRContent>
      </TRContainer>
    </TRLayout>
  );
};

export const CommonLayout: Story = {
  render: () => <ThemeLayout />,
};

export const siderLayout: Story = {
  render: () => (
    <TRLayout>
      <TRSider>
        <h1 className="navbar-brand navbar-brand-autodark">
          <a href="#">侧边导航</a>
        </h1>
        <div className="collapse navbar-collapse" id="sidebar-menu">
          <ul className="navbar-nav pt-lg-3">
            <li className="nav-item">
              <a className="nav-link" href="#">
                <span className="nav-link-title">菜单1</span>
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">
                <span className="nav-link-title">菜单2</span>
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">
                <span className="nav-link-title">菜单3</span>
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">
                <span className="nav-link-title">菜单4</span>
              </a>
            </li>
          </ul>
        </div>
      </TRSider>
      <TRContainer>
        <TRHeader>顶部导航2</TRHeader>
        <TRContent>内容区域2</TRContent>
      </TRContainer>
    </TRLayout>
  ),
};
