import React from "react";
import "../../index.css";
interface ButtonProps {
  disabled?: boolean;
  onClick?: () => void;
  children?: React.ReactNode;
  style?: React.CSSProperties;
  type?:
    | "primary"
    | "secondary"
    | "success"
    | "warning"
    | "danger"
    | "info"
    | "dark"
    | "light";
  className?: string;
  color?: string;
  ghost?: boolean;
  outline?: boolean;
  pill?: boolean;
  size?: "lg" | "sm";
  icon?: boolean;
}

export const TRButton = ({
  children,
  disabled,
  style,
  type,
  size,
  ghost,
  outline,
  pill,
  className,
  color,
  icon,
  onClick,
}: ButtonProps) => {
  return (
    <button
      className={[
        "btn",
        `${disabled ? "disabled" : ""}`,
        `${size ? `btn-${size}` : ""}`,
        `${type ? `btn-${type}` : ""}`,
        `${color ? `btn-${color}` : ""}`,
        `${ghost ? `btn-ghost-${type}` : ""}`,
        `${ghost ? `btn-ghost-${size}` : ""}`,
        `${ghost ? `btn-ghost-${color}` : ""}`,
        `${pill ? `btn-pill` : ""}`,
        `${icon ? `btn-icon` : ""}`,
        `${outline ? `btn-outline-${type}` : ""}`,
        `${outline ? `btn-outline-${size}` : ""}`,
        `${outline ? `btn-outline-${color}` : ""}`,
        className
      ].join(" ")}
      style={style}
      onClick={onClick ? () => onClick() : () => {}}
    >
      {children}
    </button>
  );
};
