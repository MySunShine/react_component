import type { Meta, StoryObj } from "@storybook/react";

import { TRButton } from "./Button";

const meta: Meta<typeof TRButton> = {
  title: "基础组件/Button 按钮",
  component: TRButton,
  argTypes: {
    disabled: {
      description: "设置按钮失效状态",
    },
    style: {
      description: "语义化结构 style",
    },
    type: {
      description: "设置按钮类型",
    },
    size: {
      description: "设置按钮大小",
    },
    ghost: {
      description: "幽灵属性，使按钮背景透明",
    },
    outline: {
      description: "设置按钮简约轮廓属性",
    },
    pill: {
      description: "设置按钮圆角效果",
    },
    className: {
      description: "自定义类名",
    },
    color: {
      description: "设置按钮颜色",
    },
    icon: {
      description: "设置按钮的图标组件",
    },
  },
};
export default meta;

type Story = StoryObj<typeof meta>;
export const CommonButton: Story = {
  render: () => (
    <div className="min-vh-100 py-4 px-4 d-flex justify-content-center align-items-center flex-wrap">
      {/* 普通type */}
      <div className="space-x">
        <TRButton type="primary">primary</TRButton>
        <TRButton type="secondary">secondary</TRButton>
        <TRButton type="success">success</TRButton>
        <TRButton type="warning">warning</TRButton>
        <TRButton type="danger">danger</TRButton>
        <TRButton type="info">info</TRButton>
        <TRButton type="light">light</TRButton>
      </div>
      {/* 禁用disabled */}
      <div className="space-x">
        <TRButton type="primary" disabled={true}>
          primary
        </TRButton>
        <TRButton type="secondary" disabled={true}>
          secondary
        </TRButton>
        <TRButton type="success" disabled={true}>
          success
        </TRButton>
        <TRButton type="warning" disabled={true}>
          warning
        </TRButton>
        <TRButton type="danger" disabled={true}>
          danger
        </TRButton>
        <TRButton type="info" disabled={true}>
          info
        </TRButton>
        <TRButton type="light" disabled={true}>
          light
        </TRButton>
      </div>
      {/* 颜色 color */}
      <div className="space-x">
        <TRButton color="blue">blue</TRButton>
        <TRButton color="azure">azure</TRButton>
        <TRButton color="indigo">indigo</TRButton>
        <TRButton color="purple">purple</TRButton>
        <TRButton color="pink">pink</TRButton>
        <TRButton color="orange">orange</TRButton>
        <TRButton color="yellow">yellow</TRButton>
      </div>
      {/* 幽灵 ghost */}
      <div className="space-x">
        <TRButton type="primary" ghost={true}>
          primary
        </TRButton>
        <TRButton type="secondary" ghost={true}>
          secondary
        </TRButton>
        <TRButton type="success" ghost={true}>
          success
        </TRButton>
        <TRButton type="warning" ghost={true}>
          warning
        </TRButton>
        <TRButton type="danger" ghost={true}>
          danger
        </TRButton>
        <TRButton type="info" ghost={true}>
          info
        </TRButton>
        <TRButton type="light" ghost={true}>
          light
        </TRButton>
      </div>
      {/* 药丸pill */}
      <div className="space-x">
        <TRButton type="primary" pill={true}>
          primary
        </TRButton>
        <TRButton type="secondary" pill={true}>
          secondary
        </TRButton>
        <TRButton type="success" pill={true}>
          success
        </TRButton>
        <TRButton type="warning" pill={true}>
          warning
        </TRButton>
        <TRButton type="danger" pill={true}>
          danger
        </TRButton>
        <TRButton type="info" pill={true}>
          info
        </TRButton>
        <TRButton type="light" pill={true}>
          light
        </TRButton>
      </div>
      {/* 大纲outline */}
      <div className="space-x">
        <TRButton type="primary" outline={true}>
          primary
        </TRButton>
        <TRButton type="secondary" outline={true}>
          secondary
        </TRButton>
        <TRButton type="success" outline={true}>
          success
        </TRButton>
        <TRButton type="warning" outline={true}>
          warning
        </TRButton>
        <TRButton type="danger" outline={true}>
          danger
        </TRButton>
        <TRButton type="info" outline={true}>
          info
        </TRButton>
        <TRButton type="light" outline={true}>
          light
        </TRButton>
      </div>
      {/* 大小 size */}
      <div className="space-x">
        <TRButton type="primary" size="lg">
          primary
        </TRButton>
        <TRButton type="secondary" size="lg">
          secondary
        </TRButton>
        <TRButton type="success" size="lg">
          success
        </TRButton>
        <TRButton type="warning" size="sm">
          warning
        </TRButton>
        <TRButton type="danger" size="sm">
          danger
        </TRButton>
        <TRButton type="info" size="sm">
          info
        </TRButton>
        <TRButton type="light" size="sm">
          light
        </TRButton>
      </div>
      {/* 图标 icon */}
      <div className="space-x">
        <TRButton>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-arrow-bar-up"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M12 4l0 10"></path>
            <path d="M12 4l4 4"></path>
            <path d="M12 4l-4 4"></path>
            <path d="M4 20l16 0"></path>
          </svg>
          Upload
        </TRButton>
        <TRButton type="warning">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-circles-relation"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M9.183 6.117a6 6 0 1 0 4.511 3.986"></path>
            <path d="M14.813 17.883a6 6 0 1 0 -4.496 -3.954"></path>
          </svg>
          Link
        </TRButton>
        <TRButton type="success">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-heart"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M19.5 12.572l-7.5 7.428l-7.5 -7.428a5 5 0 1 1 7.5 -6.566a5 5 0 1 1 7.5 6.572"></path>
          </svg>
          Like
        </TRButton>
        <TRButton type="primary">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-plus"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M12 5l0 14"></path>
            <path d="M5 12l14 0"></path>
          </svg>
          more
        </TRButton>
        <TRButton color="black">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-brand-github"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M9 19c-4.3 1.4 -4.3 -2.5 -6 -3m12 5v-3.5c0 -1 .1 -1.4 -.5 -2c2.8 -.3 5.5 -1.4 5.5 -6a4.6 4.6 0 0 0 -1.3 -3.2a4.2 4.2 0 0 0 -.1 -3.2s-1.1 -.3 -3.5 1.3a12.3 12.3 0 0 0 -6.2 0c-2.4 -1.6 -3.5 -1.3 -3.5 -1.3a4.2 4.2 0 0 0 -.1 3.2a4.6 4.6 0 0 0 -1.3 3.2c0 4.6 2.7 5.7 5.5 6c-.6 .6 -.6 1.2 -.5 2v3.5"></path>
          </svg>
          github
        </TRButton>
        <TRButton type="info">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-message-dots"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M4 21v-13a3 3 0 0 1 3 -3h10a3 3 0 0 1 3 3v6a3 3 0 0 1 -3 3h-9l-4 4"></path>
            <path d="M12 11l0 .01"></path>
            <path d="M8 11l0 .01"></path>
            <path d="M16 11l0 .01"></path>
          </svg>
          comment
        </TRButton>
      </div>
      {/* 单独图标 */}
      <div className="space-x">
        <TRButton icon={true}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-activity"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M3 12h4l3 8l4 -16l3 8h4"></path>
          </svg>
        </TRButton>
        <TRButton type="warning" icon={true}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-alert-triangle"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M10.24 3.957l-8.422 14.06a1.989 1.989 0 0 0 1.7 2.983h16.845a1.989 1.989 0 0 0 1.7 -2.983l-8.423 -14.06a1.989 1.989 0 0 0 -3.4 0z"></path>
            <path d="M12 9v4"></path>
            <path d="M12 17h.01"></path>
          </svg>
        </TRButton>
        <TRButton type="success" icon={true}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-star"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M12 17.75l-6.172 3.245l1.179 -6.873l-5 -4.867l6.9 -1l3.086 -6.253l3.086 6.253l6.9 1l-5 4.867l1.179 6.873z"></path>
          </svg>
        </TRButton>
        <TRButton type="primary" icon={true}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-trash"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M4 7l16 0"></path>
            <path d="M10 11l0 6"></path>
            <path d="M14 11l0 6"></path>
            <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12"></path>
            <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3"></path>
          </svg>
        </TRButton>
        <TRButton color="black" icon={true}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-brand-github"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M9 19c-4.3 1.4 -4.3 -2.5 -6 -3m12 5v-3.5c0 -1 .1 -1.4 -.5 -2c2.8 -.3 5.5 -1.4 5.5 -6a4.6 4.6 0 0 0 -1.3 -3.2a4.2 4.2 0 0 0 -.1 -3.2s-1.1 -.3 -3.5 1.3a12.3 12.3 0 0 0 -6.2 0c-2.4 -1.6 -3.5 -1.3 -3.5 -1.3a4.2 4.2 0 0 0 -.1 3.2a4.6 4.6 0 0 0 -1.3 3.2c0 4.6 2.7 5.7 5.5 6c-.6 .6 -.6 1.2 -.5 2v3.5"></path>
          </svg>
        </TRButton>
        <TRButton type="info" icon={true}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-brand-stackshare"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M19 6m-2 0a2 2 0 1 0 4 0a2 2 0 1 0 -4 0"></path>
            <path d="M19 18m-2 0a2 2 0 1 0 4 0a2 2 0 1 0 -4 0"></path>
            <path d="M5 12m-2 0a2 2 0 1 0 4 0a2 2 0 1 0 -4 0"></path>
            <path d="M7 12h3l3.5 6h3.5"></path>
            <path d="M17 6h-3.5l-3.5 6"></path>
          </svg>
        </TRButton>
      </div>
    </div>
  ),
};
