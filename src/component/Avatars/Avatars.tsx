import React from "react";
import '../../index.css'
interface AvatarsProps {
  children?: React.ReactNode;
  style?: React.CSSProperties;
  className?: string;
  color?: string; //颜色
  size?: "lg" | "sm" | "xl" | "xs" | "md"; //头像大小
  rounded?: boolean; //头像弧度
  cover?: any;
}
export const TRAvatars = ({
  style,
  className,
  color,
  children,
  size,
  rounded,
  cover,
}: AvatarsProps) => {
  return (
    <span
      className={[
        "avatar",
        className,
        `${color ? `bg-${color}-lt` : ""}`,
        `${size ? `avatar-${size}` : ""}`,
        `${rounded ? `rounded` : ""}`,
      ].join(" ")}
      style={{ ...style, backgroundImage: `url(${cover})` }}
    >
      {children}
    </span>
  );
};

export const TRAvatarsList = ({ className, children, style }: AvatarsProps) => {
  return (
    <div
      className={["avatar-list", "avatar-list-stacked", className].join(" ")}
      style={style}
    >
      {children}
    </div>
  );
};
