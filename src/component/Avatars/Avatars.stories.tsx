import type { Meta, StoryObj } from "@storybook/react";
import ea from "../../images/ava.jpg";
import { TRAvatars, TRAvatarsList } from "./Avatars";
const meta: Meta<typeof TRAvatars> = {
  title: "展示组件/TRAvatars 头像",
  component: TRAvatars,
  argTypes: {
    style: { description: "指定样式" },
    className: {
      description: "容器 className",
    },
    color:{
      description:'自定义头像颜色'
    },
    size:{
      description:'自定义头像大小'
    },
    cover:{
      description:'自定义头像图片'
    },
    rounded:{
      description:'头像圆形展示'
    }

  },
};
export default meta;

type Story = StoryObj<typeof meta>;

export const simpleAvatars: Story = {
  render: () => (
    <div>
      <div className="space-x">
        <TRAvatars color="green">EB</TRAvatars>
        <TRAvatars color="red">EB</TRAvatars>
        <TRAvatars color="yellow">EB</TRAvatars>
        <TRAvatars color="purple">EB</TRAvatars>
        <TRAvatars color="pink">EB</TRAvatars>
      </div>
      <div className="space-x" style={{ margin: "10px 0" }}>
        <TRAvatars size="xl">EB</TRAvatars>
        <TRAvatars size="lg">EB</TRAvatars>
        <TRAvatars size="md">EB</TRAvatars>
        <TRAvatars size="sm">EB</TRAvatars>
        <TRAvatars size="xs">EB</TRAvatars>
      </div>
      <div className="space-x" style={{ margin: "10px 0" }}>
        <TRAvatars cover={ea}>EB</TRAvatars>
        <TRAvatars cover={ea}>EB</TRAvatars>
        <TRAvatars cover={ea}>EB</TRAvatars>
        <TRAvatars cover={ea}>EB</TRAvatars>
        <TRAvatars cover={ea}></TRAvatars>
      </div>
      <div className="space-x" style={{ margin: "10px 0" }}>
        <TRAvatars rounded={true}>EB</TRAvatars>
      </div>
      <div className="space-x">
        <TRAvatars>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="icon icon-tabler icon-tabler-user-plus"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M8 7a4 4 0 1 0 8 0a4 4 0 0 0 -8 0"></path>
            <path d="M16 19h6"></path>
            <path d="M19 16v6"></path>
            <path d="M6 21v-2a4 4 0 0 1 4 -4h4"></path>
          </svg>
        </TRAvatars>
      </div>
    </div>
  ),
};

export const AvatarList: Story = {
  render: () => (
    <TRAvatarsList>
      <TRAvatars>EB</TRAvatars>
      <TRAvatars cover={ea}></TRAvatars>
      <TRAvatars cover={ea}></TRAvatars>
      <TRAvatars cover={ea}></TRAvatars>
      <TRAvatars cover={ea}></TRAvatars>
      <TRAvatars>+8</TRAvatars>
    </TRAvatarsList>
  ),
};
