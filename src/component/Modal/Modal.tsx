import React, { useState } from "react";
import "../../index.css";
import Button from "../Button";
interface ModalProp {
  style?: React.CSSProperties;
  className?: string;
  open?: boolean; //模态框打开
  title?: React.ReactNode; //模态框标题
  footer?: React.ReactNode; //模态框操作按钮
  onOk?: () => void;
  onCancel?: () => void;
  children?: React.ReactNode;
  okText?: string;
  cancelText?: string;
  closeIcon?: React.ReactNode;
}
export const TRModal = ({
  style,
  className,
  open,
  onCancel,
  title,
  footer,
  onOk,
  children,
  okText,
  cancelText,
  closeIcon,
}: ModalProp) => {
  return (
    <div
      className={["modal", open ? "showModal" : null, className].join(" ")}
      style={style}
      role="dialog"
      aria-modal="true"
      id="exampleModal"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">{title}</h5>
            {closeIcon === null ? (
              ""
            ) : closeIcon ? (
              closeIcon
            ) : (
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
                onClick={onCancel ? () => onCancel() : () => {}}
              ></button>
            )}
          </div>
          <div className="modal-body">{children}</div>
          {footer === null ? (
            ""
          ) : (
            <div className="modal-footer">
              <Button
                color="white"
                onClick={onCancel ? () => onCancel() : () => {}}
              >
                {cancelText ? cancelText : "取消"}
              </Button>
              <Button type="primary" onClick={onOk ? () => onOk() : () => {}}>
                {okText ? okText : "确定"}
              </Button>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
