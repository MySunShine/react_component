import type { Meta, StoryObj } from "@storybook/react";

import React, { useState } from "react";

import { TRModal } from "./Modal";
import Button from "../Button";

const meta: Meta<typeof TRModal> = {
  title: "操作反馈/Modal 模态框",
  component: TRModal,
  argTypes: {
    style: { description: "指定样式" },
    className: {
      description: "容器 className",
    },
    open: { description: "对话框是否展示" },
    onCancel: { description: "点击遮罩层或右上角叉或取消按钮的回调" },
    title: { description: "" },
    footer: {
      description: "底部内容，当不需要默认底部按钮时，可以设为 footer={null}",
    },
    onOk: { description: "点击确定回调" },
    children: { description: "" },
    okText: { description: "确认按钮文字" },
    cancelText: { description: "取消按钮文字" },
    closeIcon: { description: "自定义关闭图标。设置为 null时隐藏关闭按钮" },
  },
};
export default meta;
type Story = StoryObj<typeof meta>;
// React 组件名称必须以大写字母开头
const OpenModal = () => {
  const [open, setopen] = useState(false);
  const clickB = () => {
    setopen(true);
  };
  const cancelModal = () => {
    setopen(false);
  };
  const okModal = () => {
    setopen(false);
  };
  return (
    <div>
      <Button onClick={() => clickB()}>简单模态框</Button>
      <TRModal
        title="Modal title"
        open={open}
        onCancel={() => cancelModal()}
        onOk={() => okModal()}
      >
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </TRModal>
    </div>
  );
};

const OpenModal1 = () => {
  const [open, setopen] = useState(false);
  const clickB = () => {
    setopen(true);
  };
  const cancelModal = () => {
    setopen(false);
  };
  const okModal = () => {
    setopen(false);
  };
  return (
    <div>
      <Button onClick={() => clickB()}>自定义取消确认文字</Button>
      <TRModal
        title="Modal title"
        open={open}
        onCancel={() => cancelModal()}
        onOk={() => okModal()}
        okText="同意"
        cancelText="拒绝"
      >
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </TRModal>
    </div>
  );
};

const OpenModal2 = () => {
  const [open, setopen] = useState(false);
  const clickB = () => {
    setopen(true);
  };
  const cancelModal = () => {
    setopen(false);
  };
  const okModal = () => {
    setopen(false);
  };
  return (
    <div>
      <Button onClick={() => clickB()}>自定义右上角取消图表</Button>
      <TRModal
        title="Modal title"
        open={open}
        onCancel={() => cancelModal()}
        onOk={() => okModal()}
        okText="同意"
        cancelText="拒绝"
        closeIcon={null}
      >
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </TRModal>
    </div>
  );
};

export const simpleModal: Story = {
  render: () => (
    <div className="space-y">
      <OpenModal />
      <OpenModal1 />
      <OpenModal2 />
    </div>
  ),
};
