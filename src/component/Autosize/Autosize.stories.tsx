import type { Meta, StoryObj } from "@storybook/react";

import { TRAutoSize } from "./Autosize";

const meta: Meta<typeof TRAutoSize> = {
  title: "数据录入/AutoSize 多行文本框",
  component: TRAutoSize,
  argTypes: {
    style: { description: "指定样式" },
    className: {
      description: "容器 className",
    },
    placeholder:{
      description:'未输入时默认内容'
    }
  },
};
export default meta;

type Story = StoryObj<typeof meta>;

export const simpleAutoSize: Story = {
  render: () => <TRAutoSize placeholder="请输入你的问题"/>,
};
