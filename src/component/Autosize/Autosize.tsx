import React from "react";
import '../../index.css'
interface AutosizeProps {
  onClick?: () => void;
  children?: React.ReactNode;
  style?: React.CSSProperties;
  className?: string;
  placeholder?: string; //初始值
}
export const TRAutoSize = ({ style, className, placeholder }: AutosizeProps) => {
  return (
    <textarea
      className={["form-control", className].join(" ")}
      data-bs-toggle="autosize"
      placeholder={placeholder}
      style={style}
    >
      
    </textarea>
  );
};
