import React from "react";
import "../../index.css";
interface CardsProps {
  children?: React.ReactNode;
  style?: React.CSSProperties;
  status?: "top" | "start"; //状态条位置
  size?: "lg" | "sm" | "xl" | "xs" | "md"; //卡片大小
  statuscolor?: string; //状态条颜色
  className?: string; //类名
  cover?: any; //图片
  Stacked?: boolean; //堆叠状态
  imgPos?: "top" | "bottom"; //图片位置
}

interface CardProps {
  children?: React.ReactNode;
  style?: React.CSSProperties;
  className?: string; //类名
}

export const TRCardTitle = ({ style, className, children }: CardProps) => {
  return (
    <div className={["card-title", className].join(" ")} style={style}>
      {children}
    </div>
  );
};

export const TRCardBody = ({ style, className, children }: CardProps) => {
  return (
    <div className={["card-body", className].join(" ")} style={style}>
      {children}
    </div>
  );
};

export const TRCardHead = ({ style, className, children }: CardProps) => {
  return (
    <div className={["card-header", className].join(" ")} style={style}>
      {children}
    </div>
  );
};

export const TRCards = ({
  style,
  className,
  children,
  status,
  statuscolor,
  size,
  cover,
  Stacked,
  imgPos,
}: CardsProps) => {
  return (
    <div
      className={[
        "card",
        className,
        `${size ? `card-status-${size}` : ""}`,
        `${Stacked ? `card-stacked` : ""}`,
      ].join(" ")}
      style={style}
    >
      {status ? (
        <div
          className={[
            `card-status-${status}`,
            `${statuscolor ? `bg-${statuscolor}` : ""}`,
          ].join(" ")}
        ></div>
      ) : (
        ""
      )}
      {cover ? (
        <div
          className={[
            "img-responsive",
            "img-responsive-21x9",
            `card-img-${imgPos}`,
          ].join(" ")}
          style={{ backgroundImage: `url(${cover})` }}
        ></div>
      ) : (
        ""
      )}
      {children}
    </div>
  );
};
