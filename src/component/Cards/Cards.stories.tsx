import type { Meta, StoryObj } from "@storybook/react";

import { TRCards, TRCardBody, TRCardHead, TRCardTitle } from "./Cards";
import ea from "../../images/ava.jpg";
import ea1 from "../../images/ava1.jpg";
import { TRAvatars } from "../Avatars";
const meta: Meta<typeof TRCards> = {
  title: "基础组件/Cards 卡片",
  component: TRCards,
  argTypes: {
    style: {
      description: "指定样式",
    },
    className: {
      description: "自定义类名",
    },
    status: {
      description: "设置card状态条位置",
    },
    statuscolor: {
      description: "设置状态条颜色",
    },
    size: {
      description: "设置卡片大小",
    },
    Stacked: {
      description: "设置卡片堆叠效果",
    },
  },
};
export default meta;

type Story = StoryObj<typeof meta>;
export const simpleTRCards: Story = {
  render: () => (
    <div className="space-y">
      <TRCards>
        <TRCardBody>
          <p>This is some text within a card body.</p>
        </TRCardBody>
      </TRCards>
      <TRCards size="sm">
        <TRCardBody>
          <p>This is some text within a card body.</p>
        </TRCardBody>
      </TRCards>
      <TRCards>
        <TRCardBody>
          <TRCardTitle>Card title</TRCardTitle>
          <p>This is some text within a card body.</p>
        </TRCardBody>
      </TRCards>
      <TRCards>
        <TRCardHead>
          <TRCardTitle>Card title</TRCardTitle>
        </TRCardHead>
        <TRCardBody>
          <p>This is some text within a card body.</p>
        </TRCardBody>
      </TRCards>
      <TRCards size="sm" style={{ width: 300 }}>
        <div className="row">
          <div className="col-3">
            <img
              src={ea1}
              alt="左侧图片"
              className="w-100 h-100 object-cover w-100 h-100 object-cover"
            />
          </div>
          <div className="col">
            <TRCardBody>
              <TRCardTitle>Card title</TRCardTitle>
              <p>This is some text within a card body.</p>
            </TRCardBody>
          </div>
        </div>
      </TRCards>
      <TRCards size="sm" style={{ width: 300 }}>
        <div className="row">
          <div className="col">
            <TRCardBody>
              <TRCardTitle>Card title</TRCardTitle>
              <p>This is some text within a card body.</p>
            </TRCardBody>
          </div>
          <div className="col-3">
            <img
              src={ea1}
              alt="左侧图片"
              className="w-100 h-100 object-cover w-100 h-100 object-cover"
            />
          </div>
        </div>
      </TRCards>
      <TRCards size="sm" style={{ width: 300 }} cover={ea1} imgPos="top">
        <TRCardBody>
          <TRCardTitle>Card title</TRCardTitle>
          <p>This is some text within a card body.</p>
        </TRCardBody>
      </TRCards>
      <TRCards size="sm" style={{ width: 300 }} cover={ea1} imgPos="bottom">
        <TRCardBody>
          <TRCardTitle>Card title</TRCardTitle>
          <p>This is some text within a card body.</p>
        </TRCardBody>
      </TRCards>
      <TRCards size="sm" style={{ width: 300 }}>
        <TRCardBody>
          <TRCardTitle>Card title</TRCardTitle>
          <p>
            Are you suggesting that coconuts migrate? No, no, no! Yes, yes. A
            bit. But she's got a wart. You ...
          </p>
          <div className="d-flex align-items-center pt-4 mt-auto">
            <TRAvatars cover={ea}></TRAvatars>
            <div className="ms-3">
              <a href="#" className="text-body">
                Maryjo Lebarree
              </a>
              <div className="text-secondary">3 days ago</div>
            </div>
          </div>
        </TRCardBody>
      </TRCards>
      <TRCards status="top" statuscolor="red">
        <TRCardBody>
          <TRCardTitle>Card title</TRCardTitle>
          <p>This is some text within a card body.</p>
        </TRCardBody>
      </TRCards>
      <TRCards status="start" statuscolor="green">
        <TRCardBody>
          <TRCardTitle>Card title</TRCardTitle>
          <p>This is some text within a card body.</p>
        </TRCardBody>
      </TRCards>
      <TRCards Stacked={true}>
        <TRCardBody>
          <TRCardTitle>Card title</TRCardTitle>
          <p>This is some text within a card body.</p>
        </TRCardBody>
      </TRCards>
    </div>
  ),
};
