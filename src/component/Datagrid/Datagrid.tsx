import React from "react";
import '../../index.css'
interface DatagridProp {
  style?: React.CSSProperties;
  children?: React.ReactNode;
  className?: string;
  Items?: { title?: React.ReactNode; description?: React.ReactNode }[];
}

export const TRDatagrid = ({ style, className, Items }: DatagridProp) => {
  return (
    <div className={["datagrid", className].join(" ")} style={style}>
      {Items && Items.length > 0
        ? Items.map((item, i) => {
            return (
              <div className="datagrid-item" key={i}>
                {item.title ? (
                  <div className="datagrid-title">{item.title}</div>
                ) : (
                  ""
                )}
                {item.description ? (
                  <div className="datagrid-content">{item.description}</div>
                ) : (
                  ""
                )}
              </div>
            );
          })
        : ""}
    </div>
  );
};
