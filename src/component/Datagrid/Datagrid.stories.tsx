import type { Meta, StoryObj } from "@storybook/react";
import ea from "../../images/ava.jpg";
import { TRDatagrid } from "./Datagrid";
import { TRAvatars, TRAvatarsList } from "../Avatars/Avatars";
const meta: Meta<typeof TRDatagrid> = {
  title: "展示组件/Datagrid 数据栅格",
  component: TRDatagrid,
  argTypes: {
    style: { description: "指定样式" },
    className: {
      description: "容器 className",
    },
    Items: {
      description: "数据展示列表",
    },
  },
};
export default meta;

type Story = StoryObj<typeof meta>;
export const simpleDatagrid: Story = {
  render: () => (
    <TRDatagrid
      Items={[
        { title: "Registrar", description: "Third Party" },
        { title: "Nameservers", description: "Third Party" },
        {
          title: "Creator",
          description: (
            <div className="d-flex align-items-center">
              <TRAvatars cover={ea}></TRAvatars>
              Paweł Kuna
            </div>
          ),
        },
        { title: "Age", description: "15 day" },
        {
          title: "Avatars list",
          description: (
            <TRAvatarsList>
              <TRAvatars cover={ea}></TRAvatars>
              <TRAvatars cover={ea}></TRAvatars>
              <TRAvatars cover={ea}></TRAvatars>
              <TRAvatars cover={ea}></TRAvatars>
              <TRAvatars>+n</TRAvatars>
            </TRAvatarsList>
          ),
        },
        { title: "Port number", description: "3306" },
      ]}
    ></TRDatagrid>
  ),
};
