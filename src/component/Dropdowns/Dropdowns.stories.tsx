import type { Meta, StoryObj } from "@storybook/react";
import { TRDropdowns } from "./Dropdowns";
import { TRCards, TRCardBody, TRCardTitle } from "../Cards/Cards";
import ea from "../../images/ava.jpg";
import ea1 from "../../images/ava1.jpg";
import {TRAvatars} from "../Avatars";
const meta: Meta<typeof TRDropdowns> = {
  title: "导航组件/Dropdowns 下拉",
  component: TRDropdowns,
  argTypes: {
    style: { description: "指定样式" },
    className: {
      description: "容器 className",
    },
    value: {
      description: "下拉框选中的值",
    },
    defaultValue: {
      description: "默认下拉展示内容",
    },
    Items: {
      description: "下拉菜单配置项",
    },
    menuHead: {
      description: "下拉菜单顶部标题",
    },
    arrow: {
      description: "设置下拉菜单指示箭头",
    },
    dark: {
      description: "设置下拉菜单黑色主体",
    },
    onChange: {
      description: "下拉菜单选项切换回调函数",
    },
  },
};
export default meta;

type Story = StoryObj<typeof meta>;

export const simpleDropdowns: Story = {
  render: () => (
    <div className="space-y">
      {/* 普通下拉框 */}
      <TRDropdowns
        Items={[
          { label: "Action", value: "Action" },
          { label: "Another action", value: "Another action" },
          { label: "Action", value: "Third action" },
        ]}
        defaultValue="普通下拉框"
      ></TRDropdowns>
      {/* 有默认值 */}
      <TRDropdowns
        Items={[
          { label: "Action", value: "Action" },
          { label: "Another action", value: "Another action" },
          { label: "Action", value: "Third action" },
        ]}
        defaultValue="默认值"
        value="Action"
      ></TRDropdowns>
      {/* 有废弃禁用 */}
      <TRDropdowns
        Items={[
          { label: "Action", value: "Action" },
          { label: "Another action", value: "Another action" },
          { label: "Action", value: "Third action", disabled: true },
        ]}
        defaultValue="废弃禁用"
      ></TRDropdowns>
      {/* 有指示箭头 */}
      <TRDropdowns
        Items={[
          { label: "Action", value: "Action" },
          { label: "Another action", value: "Another action" },
          { label: "Action", value: "Third action", disabled: true },
        ]}
        defaultValue="指示箭头"
        arrow={true}
      ></TRDropdowns>
      {/* 黑色主题 */}
      <TRDropdowns
        Items={[
          { label: "Action", value: "Action" },
          { label: "Another action", value: "Another action" },
          { label: "Action", value: "Third action" },
        ]}
        defaultValue="黑色主题"
        dark={true}
      ></TRDropdowns>
      {/* 菜单表头 */}
      <TRDropdowns
        Items={[
          { label: "Action", value: "Action" },
          { label: "Another action", value: "Another action" },
          { label: "Action", value: "Third action" },
        ]}
        defaultValue="有菜单表头"
        menuHead="这是菜单标题"
      ></TRDropdowns>
      {/* 有图标 */}
      <TRDropdowns
        Items={[
          {
            label: "Action",
            value: "Action",
            Icon: (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="icon icon-tabler icon-tabler-activity"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                strokeWidth="2"
                stroke="currentColor"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <path d="M3 12h4l3 8l4 -16l3 8h4"></path>
              </svg>
            ),
          },
          {
            label: "Another action",
            value: "Another action",
            Icon: (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="icon icon-tabler icon-tabler-accessible"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                strokeWidth="2"
                stroke="currentColor"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <path d="M12 12m-9 0a9 9 0 1 0 18 0a9 9 0 1 0 -18 0"></path>
                <path d="M10 16.5l2 -3l2 3m-2 -3v-2l3 -1m-6 0l3 1"></path>
                <circle cx="12" cy="7.5" r=".5" fill="currentColor"></circle>
              </svg>
            ),
          },
          {
            label: "Action",
            value: "Third action",
            Icon: (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="icon icon-tabler icon-tabler-ad"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                strokeWidth="2"
                stroke="currentColor"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <path d="M3 5m0 2a2 2 0 0 1 2 -2h14a2 2 0 0 1 2 2v10a2 2 0 0 1 -2 2h-14a2 2 0 0 1 -2 -2z"></path>
                <path d="M7 15v-4a2 2 0 0 1 4 0v4"></path>
                <path d="M7 13l4 0"></path>
                <path d="M17 9v6h-1.5a1.5 1.5 0 1 1 1.5 -1.5"></path>
              </svg>
            ),
          },
        ]}
        defaultValue="图标显示"
      ></TRDropdowns>
      {/* 下拉card */}
      <TRDropdowns defaultValue="下拉展示其余内容" arrow={true}>
      <TRCards cover={ea1} size="sm" style={{ width: 300 }}>
        <TRCardBody>
          <TRCardTitle>Card title</TRCardTitle>
          <p>
            Are you suggesting that coconuts migrate? No, no, no! Yes, yes. A
            bit. But she's got a wart. You ...
          </p>
          <div className="d-flex align-items-center pt-4 mt-auto">
            <TRAvatars cover={ea}></TRAvatars>
            <div className="ms-3">
              <a href="#" className="text-body">
                Maryjo Lebarree
              </a>
              <div className="text-secondary">3 days ago</div>
            </div>
          </div>
        </TRCardBody>
      </TRCards>
      </TRDropdowns>
    </div>
  ),
};
