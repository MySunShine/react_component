import React, { useState } from "react";
import '../../index.css'
interface DropdownProp {
  style?: React.CSSProperties;
  className?: string;
  defaultValue?: string;
  value?: string;
  selectKey?: string;
  arrow?: boolean; //指引箭头
  Items?: {
    label: React.ReactNode;
    value: string;
    Icon?: React.ReactNode;
    child?: React.ReactNode;
    disabled?: boolean;
  }[]; //菜单项
  menuHead?: string;
  dark?: boolean; //黑色主体
  onChange?: (e: any) => void;
  children?: React.ReactNode;
}

export const TRDropdowns = ({
  style,
  className,
  value,
  defaultValue,
  Items,
  menuHead,
  arrow,
  dark,
  onChange,
  children,
}: DropdownProp) => {
  const [show, setShow] = useState(false);
  const [ckey, setckey] = useState(value);
  const showMenu = () => {
    setShow(!show);
  };
  const selectKey = (key: any) => {
    setckey(key);
    setShow(!show);
  };
  return (
    <div className={["dropdown", className].join(" ")} style={style}>
      <div
        className={["btn", "dropdown-toggle", `${show ? "show" : ""}`].join(
          " "
        )}
        data-bs-toggle="dropdown"
        onClick={() => showMenu()}
      >
        {ckey ? ckey : defaultValue ? defaultValue : ""}
      </div>
      <div
        className={[
          "dropdown-menu",
          `${arrow ? "dropdown-menu-arrow" : ""}`,
          `${dark ? "bg-dark text-white" : ""}`,
          `${show ? "show" : ""}`,
        ].join(" ")}
        onClick={onChange ? () => onChange(ckey) : () => {}}
      >
        {menuHead ? <span className="dropdown-header">{menuHead}</span> : ""}
        {Items && Items.length > 0
          ? Items.map((item) => {
              return (
                <div
                  className={[
                    "dropdown-item",
                    `${ckey && ckey === item.value ? "active" : ""}`,
                    `${item.disabled ? "disabled" : ""}`,
                  ].join(" ")}
                  key={item.value}
                  onClick={() => selectKey(item.value)}
                >
                  {item.Icon ? item.Icon : ""}
                  {item.value}
                  {item.child ? item.child : ""}
                </div>
              );
            })
          : ""}
        {children ? children : ""}
      </div>
    </div>
  );
};
