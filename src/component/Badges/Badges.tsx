import React from "react";
import '../../index.css'
interface BadgesProps {
  children?: React.ReactNode;
  style?: React.CSSProperties;
  className?: string;
  bgcolor?: string; //背景色颜色
  textcolor?: string; //字体颜色
  pill?: boolean; //药丸状态
  softcolor?: string; //舒服色
  outline?: boolean; //外框
}
export const TRBadges = ({
  style,
  className,
  bgcolor,
  textcolor,
  pill,
  softcolor,
  outline,
  children,
}: BadgesProps) => {
  return (
    <span
      style={style}
      className={[
        "badge",
        className,
        `${bgcolor ? `bg-${bgcolor}` : ""}`,
        `${outline ? `badge-outline` : ""}`,
        `${textcolor ? `text-${textcolor}` : ""}`,
        `${softcolor ? `bg-${softcolor}-lt` : ""}`,
        `${pill ? `badge-pill` : ""}`,
      ].join(" ")}
    >
      {children}
    </span>
  );
};
