import type { Meta, StoryObj } from "@storybook/react";

import { TRBadges } from "./Badges";

const meta: Meta<typeof TRBadges> = {
  title: "展示组件/Badges 徽标",
  component: TRBadges,
  argTypes: {
    style: { description: "指定样式" },
    className: {
      description: "容器 className",
    },
    bgcolor:{
      description:'徽标背景色'
    },
    textcolor:{
      description:'徽标字体颜色'
    },
    pill:{
      description:'徽标药丸效果圆角'
    },
    softcolor:{
      description:"柔和的徽标配色"
    },
    outline:{
      description:'简约线条徽标'
    },
  },
};
export default meta;

type Story = StoryObj<typeof meta>;

export const simpleBadges: Story = {
  render: () => (
    <div className="space-y">
      <div className="space-x">
        <TRBadges bgcolor="blue">blue</TRBadges>
        <TRBadges bgcolor="azure">Azure</TRBadges>
        <TRBadges bgcolor="indigo">Indigo</TRBadges>
        <TRBadges bgcolor="purple">Purple</TRBadges>
        <TRBadges bgcolor="pink">Pink</TRBadges>
        <TRBadges bgcolor="red">Red</TRBadges>
      </div>
      <div className="space-x">
        <TRBadges textcolor="blue" outline={true}>Blue</TRBadges>
        <TRBadges textcolor="azure" outline={true}>Azure</TRBadges>
        <TRBadges textcolor="indigo" outline={true}>Indigo</TRBadges>
        <TRBadges textcolor="purple" outline={true}>Purple</TRBadges>
        <TRBadges textcolor="pink" outline={true}>Pink</TRBadges>
        <TRBadges textcolor="red" outline={true}>Red</TRBadges>
      </div>
      <div className="space-x">
        <TRBadges bgcolor="blue" pill={true}>1</TRBadges>
        <TRBadges bgcolor="azure" pill={true}>2</TRBadges>
        <TRBadges bgcolor="indigo" pill={true}>3</TRBadges>
        <TRBadges bgcolor="purple" pill={true}>4</TRBadges>
        <TRBadges bgcolor="pink" pill={true}>5</TRBadges>
        <TRBadges bgcolor="red" pill={true}>6</TRBadges>
      </div>
      <div className="space-x">
        <TRBadges softcolor="blue">Blue</TRBadges>
        <TRBadges softcolor="azure">Azure</TRBadges>
        <TRBadges softcolor="indigo">Indigo</TRBadges>
        <TRBadges softcolor="purple">Purple</TRBadges>
        <TRBadges softcolor="pink">Pink</TRBadges>
        <TRBadges softcolor="red">Red</TRBadges>
      </div>
    </div>
  ),
};
