import React, { useState } from "react";
import "../../index.css";
import { TRCardBody, TRCardTitle, TRCards, TRCardHead } from "../Cards";
import { TRFormItem } from "../Form/Form";
import { InputIcon, InputIconGroup } from "../Input";
import Button from "../Button";
interface LoginProp {
  style?: React.CSSProperties;
  className?: string;
  Items?: {
    title?: string;
    children: { key?: string; icon?: React.ReactNode; content?: string }[];
  };
  onLogin?: (e: any) => void;
}

interface objProp {
  [key: string]: string;
}

interface LogoProp {
  title?: string;
  icon?: string;
  style?: React.CSSProperties;
  className?: string;
}
export const FormArea = ({ style, className, Items, onLogin }: LoginProp) => {
  const [valueHash, setHash] = useState<objProp>({});
  const inputCon = (e: any, key: any) => {
    const obj = JSON.parse(JSON.stringify(valueHash));
    obj[key] = e.target.value;
    setHash(obj);
  };
  return (
    <TRCards className={className} style={{ width: 300, ...style }}>
      <TRCardHead>
        <TRCardTitle>{Items?.title}</TRCardTitle>
      </TRCardHead>
      <TRCardBody className="flex-col">
        {Items && Items.children && Items.children.length > 0
          ? Items.children.map((item, i) => {
              return (
                <TRFormItem key={i} style={{ width: "100%" }}>
                  <InputIconGroup key={i}>
                    <InputIcon>{item.icon}</InputIcon>
                    <input
                      type="text"
                      className="form-control form-control-md"
                      name="example-password-input"
                      placeholder={item.content}
                      onInput={(e) => inputCon(e, item.key)}
                    />
                  </InputIconGroup>
                </TRFormItem>
              );
            })
          : ""}
        <Button
          style={{ width: "100%" }}
          color="indigo"
          onClick={onLogin ? () => onLogin(valueHash) : () => {}}
        >
          登录
        </Button>
      </TRCardBody>
    </TRCards>
  );
};

export const LogoArea = ({ title, icon, style, className }: LogoProp) => {
  return (
    <div className={["logoArea", "row", className].join(" ")} style={style}>
      <div className="col-3">
        <img
          src={icon}
          alt="logo"
          className="w-100 h-100 object-cover w-100 h-100 object-cover"
        />
      </div>
      <div className="col logo_title">
        <div className="logo_spin">|</div>
        {title}
      </div>
    </div>
  );
};
