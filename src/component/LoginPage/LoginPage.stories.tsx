import type { Meta, StoryObj } from "@storybook/react";

import { FormArea, LogoArea } from "./LoginPage";
import ea from "../../images/ava.jpg";
const meta: Meta<typeof FormArea> = {
  title: "页面/FormArea 表单页",
  component: FormArea,
  argTypes: {
    style: { description: "指定样式" },
    className: {
      description: "容器 className",
    },
    Items:{
      description:'表单各输入项配置'
    }
  },
};
export default meta;

type Story = StoryObj<typeof meta>;
export const simpleLoginPage: Story = {
  render: () => (
    <FormArea
      Items={{
        title: "账号和密码登录",
        children: [
          {
            icon: (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="icon icon-tabler icon-tabler-user"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                strokeWidth="2"
                stroke="currentColor"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <path d="M8 7a4 4 0 1 0 8 0a4 4 0 0 0 -8 0"></path>
                <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
              </svg>
            ),
            key: "user",
            content: "请输入用户名",
          },
          {
            icon: (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="icon icon-tabler icon-tabler-lock"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                strokeWidth="2"
                stroke="currentColor"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <path d="M5 13a2 2 0 0 1 2 -2h10a2 2 0 0 1 2 2v6a2 2 0 0 1 -2 2h-10a2 2 0 0 1 -2 -2v-6z"></path>
                <path d="M11 16a1 1 0 1 0 2 0a1 1 0 0 0 -2 0"></path>
                <path d="M8 11v-4a4 4 0 1 1 8 0v4"></path>
              </svg>
            ),
            key: "password",
            content: "请输入用户密码",
          },
        ],
      }}
      onLogin={(e) => {}}
    />
  ),
};

export const simpleLogo: Story = {
  render: () => (
    <LogoArea title="xxxx处理系统" icon={ea} style={{ width: 300 }} />
  ),
};
