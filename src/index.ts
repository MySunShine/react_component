export {
  default as TRAlerts
} from './component/Alerts';  //提示框
export {
  default as TRAutoSize
} from './component/Autosize';  //多行文本域
export {
  TRAvatars, TRAvatarsList
} from './component/Avatars';  //头像
export {
  default as Badges
} from './component/Badges';  //徽标
export {
  default as TRButton
} from './component/Button';  //按钮组件
export {
  TRCards, TRCardBody, TRCardHead, TRCardTitle
} from './component/Cards';  //卡片
export { default as Classify } from './component/Classify'
export {
  default as TRDatagrid
} from './component/Datagrid';  //数据栅格
export {
  default as TRDropdowns
} from './component/Dropdowns';  //下拉菜单
export {
  TRForm, TRFormItem
} from './component/Form';  //表单
export {
  TRInput, InputIcon,
  InputIconGroup
} from './component/Input' //输入框
export {
  TRLayout, TRHeader, TRSider, TRContent
} from './component/layout';  //布局
export { FormArea, LogoArea } from './component/LoginPage'
export { default as TRModal } from './component/Modal'
export { default as TRMenu } from './component/Menu'






